﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoPoolOperation.Entity
{
    public class Video
    {
        public int Id { get; set; }
        public string ObjectId { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string VideoPath{ get; set; }
        public string VideoType { get; set; }
        public string Label { get; set; }
        public string Domain { get; set; }
        public bool IsActive { get; set; }
        public string ContentObjectId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool ErstreamCheck { get; set; }
        public bool MediaNovaCheck { get; set; }
        public bool SendRequest { get; set; }
    }
}
