﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoPoolOperation.Model
{
    public class ResponseModel
    {
        public Data data { get; set; }
    }
    public class VideoPath
    {
        public string video_path { get; set; }
        public string video_type { get; set; }
        public string label { get; set; }
    }
    public class Item
    {
        public string _id { get; set; }
        public string slug { get; set; }
        public List<VideoPath> video_paths { get; set; }
        public string title { get; set; }
    }
    public class Data
    {
        public int count { get; set; }
        public List<Item> items { get; set; }
    }
    public class ObjectResponse
    {
        public ObjectIdResponse data { get; set; }
    }
    public class ObjectIdResponse
    {
        public int count { get; set; }
        public List<ObjectClass> items { get; set; }
    }
    public class ObjectClass
    {
        public string _id { get; set; }
    }
}
