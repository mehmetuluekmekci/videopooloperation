﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace VideoPoolOperation.Model
{
	public class CreateTokenRequestModel
	{
		[JsonProperty("username")]
		public string Username { get; set; }
		[JsonProperty("password")]

		public string Password { get; set; }
	}
	public class TokenResponseModel
	{
		[JsonProperty("token")]
		public string Token { get; set; }
	}
}
