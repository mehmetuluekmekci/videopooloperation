﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace VideoPoolOperation.Services.Abstract
{
    public interface IRepositoryService
    {
        T Get<T>(Expression<Func<T, bool>> condition) where T : class;
        List<T> GetList<T>(Expression<Func<T, bool>> condition, int take, int skip) where T : class;
        int Add<T>(T entity) where T : class;
        int Delete<T>(T entity) where T : class;
        int Update<T>(T entity) where T : class;

        int Count<T>(Expression<Func<T, bool>> condition=null) where T : class;
    }
}
