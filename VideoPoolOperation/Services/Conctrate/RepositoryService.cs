﻿using VideoPoolOperation.Core;
using VideoPoolOperation.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace VideoPoolOperation.Services
{
    public class RepositoryService : IRepositoryService
    {
        private readonly VideoDbContext _videoDbContext;
        public RepositoryService(VideoDbContext videoDbContext)
        {
            _videoDbContext = videoDbContext;
        }
        public int Add<T>(T entity) where T : class
        {
            var activeEntity = _videoDbContext.Add(entity);
            activeEntity.State = EntityState.Added;
            return _videoDbContext.SaveChanges();
        }

        public int Count<T>(Expression<Func<T, bool>> condition=null) where T : class
        {
            return condition == null ?
                _videoDbContext.Set<T>().Count():
                _videoDbContext.Set<T>().Where(condition).Count();
        }

        public int Delete<T>(T entity) where T : class
        {
            var activeEntity = _videoDbContext.Entry(entity);
            activeEntity.State = EntityState.Deleted;
            return  _videoDbContext.SaveChanges();
        }

        public T Get<T>(Expression<Func<T, bool>> condition) where T : class
        {
            return _videoDbContext.Set<T>().FirstOrDefault(condition);
        }

        public List<T> GetList<T>(Expression<Func<T, bool>> condition,int take,int skip) where T : class
        {
           return condition == null ?
                 _videoDbContext.Set<T>().ToList() :
                 _videoDbContext.Set<T>().Where(condition).Skip(skip).Take(take).ToList();
        }

        public int Update<T>(T entity) where T : class
        {
            var activeEntity = _videoDbContext.Entry(entity);
            activeEntity.State = EntityState.Modified;
            return _videoDbContext.SaveChanges();
        }
    }
}
