﻿using VideoPoolOperation.Core;
using VideoPoolOperation.Entity;
using VideoPoolOperation.Model;
using VideoPoolOperation.Services;
using VideoPoolOperation.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using OfficeOpenXml;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;

namespace VideoPoolOperation
{
    class Program
    {
        public static string Token { get; set; }

        const string Username = "mehmetali";

        const string Password = "123456";

        const string CurrentDomainName = "Posta";

        const string DomainId = "5a20015687644da59c92064e";


        //Fanatik Canlı 5ad3d998ae298bd7ea46c9e0
        //Fanatik Stage 5a61af791e6bbc48583cbfbc
        //Posta Canlı 5a20015687644da59c92064e
        //Posta Stage 5aa8d3e24747152c4823fd23

        static void Main(string[] args)
        {
            GetPoolandInsertVideo(82462);
           // VideoContentRelationNewsFinder();
          // ExcelParse();
        }

        #region Operation
        /// <summary>
        /// Cms üzeriden test sorgu datasource'dan data çeker ve sql db'ye yazar.Kodun en altında data source query'si bulunmaktadır.
        /// </summary>
        /// <param name="totalContentSize"></param>
        public static void GetPoolandInsertVideo(int totalContentSize)
        {
            //i başlangıç değeri son  çekdiğiniz page'den  devam ederek çekebilirsiniz son değer  110 i=110 dan başlanabilir.Db'de var mı diye kontrol ediyor
            const int pageSize = 500;
            int count= totalContentSize / pageSize+1;
            for (int i = 163; i <= pageSize; i++)
            {
                Console.WriteLine(i);
                int skip = pageSize * (i - 1);
                var videos = GetData(skip, pageSize);
                if (videos != null && videos.Any())
                {
                    InsertOperation(videos);
                }
                else
                {
                    throw new ArgumentException("Datasourcde'dan data alınamadı!");
                }
            }
        }

        /// <summary>
        ///cms'e istek atarak video object Id hangi haberlerde var onu bulup sql dbye yazan  method
        /// </summary>
        public static void VideoContentRelationNewsFinder()
        {
            try
            {
                var serviceProvider = new ServiceCollection()
                .AddSingleton<IRepositoryService, RepositoryService>()
                .AddDbContext<VideoDbContext>()
                .BuildServiceProvider();
                var _repositoryService = serviceProvider.GetRequiredService<IRepositoryService>();
                int count = _repositoryService.Count<Video>(x => x.Domain == CurrentDomainName);
                const int pageSize = 2000;
                int pageCount = count / pageSize;
                for (int i = 1; i <= pageCount; i++)
                {
                    int skip = (i - 1) * pageSize;
                    var videoList = _repositoryService.GetList<Video>(x => x.Domain == CurrentDomainName, pageSize, skip).GroupBy(x => x.ObjectId);

                    foreach (var item in videoList)
                    {
                        var objectId = GetContentObjectId(item.Key);
                        foreach (var root in item)
                        {
                            if (!string.IsNullOrEmpty(objectId))
                            {
                                root.ContentObjectId = objectId;
                                root.IsActive = true;
                            }
                            else
                            {
                                root.IsActive = false;
                            }

                            root.UpdateDate = DateTime.Now;
                            Update(root);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        #endregion

        #region  CmsMethod
        public static List<Item> GetData(int skip, int take)
        {
            try
            {
                var client = new RestClient($"http://delivery.dogannet.tv/api/domains/{DomainId}/datasources/testsorgu/result?skip={skip}&limit={take}");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", GetToken());
                request.AddHeader("Content-Type", "application/json");
                IRestResponse response = client.Execute(request);
                ResponseModel responseModel = new ResponseModel();
                if (response.IsSuccessful)
                {
                    responseModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseModel>(response.Content);
                }
                else
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        GetRefreshToken();
                        return GetData(skip, take);
                    }
                    if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        return null;
                    }
                }
                return responseModel?.data?.items;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new List<Item>();
            }

        }
        public static string GetContentObjectId(string objectId)
        {
            try
            {
                if (!string.IsNullOrEmpty(objectId))
                {
                    string body = string.Format("{0}{1}{2}", "{\n\"where\": {\n\"video._id\":\"", objectId, "\"\n\t},\n\t\"select\": {\n\t\t\"id\": 1\n\t}\n}");
                    var client = new RestClient($"http://delivery.dogannet.tv/api/domains/{DomainId}/contents/_query");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Authorization", GetToken());
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (response.IsSuccessful)
                    {
                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            return "";
                        }
                        ObjectResponse responseModel = Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectResponse>(response.Content);
                        if (responseModel.data != null && responseModel.data.items.Count > 0)
                            return responseModel.data.items.FirstOrDefault()._id;
                        else
                            return string.Empty;
                    }
                    else
                    {
                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            return "";
                        }
                        if (response.StatusCode == HttpStatusCode.NotFound)
                        {
                            return "";
                        }

                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return string.Empty;
            }


        }
        public static string GetToken()
        {
            if (string.IsNullOrWhiteSpace(Token))
            {

                CreateTokenRequestModel body = new CreateTokenRequestModel()
                {
                    Username = Username,
                    Password = Password
                };
                string authModelJson = JsonConvert.SerializeObject(body);
                var client = new RestClient("http://management.dogannet.tv/api/tokens");
                RestRequest request = new RestRequest(Method.POST);
                request.AddParameter("raw", authModelJson, ParameterType.RequestBody);
                using (CancellationTokenSource cancellationTokenSource = new CancellationTokenSource())
                {
                    IRestResponse response = client.Execute(request);

                    if (!response.IsSuccessful)
                    {
                        throw new ArgumentException("Token alınamadı");
                    }

                    Token = "Bearer " + JsonConvert.DeserializeObject<TokenResponseModel>(response.Content).Token;
                    return Token;
                }
            }
            return Token;
        }
        public static void GetRefreshToken()
        {

            CreateTokenRequestModel body = new CreateTokenRequestModel()
            {
                Username = Username,
                Password = Password
            };
            string authModelJson = JsonConvert.SerializeObject(body);
            var client = new RestClient("http://management.dogannet.tv/api/tokens");
            RestRequest request = new RestRequest(Method.POST);
            request.AddParameter("raw", authModelJson, ParameterType.RequestBody);
            using (CancellationTokenSource cancellationTokenSource = new CancellationTokenSource())
            {
                IRestResponse response = client.Execute(request);

                if (!response.IsSuccessful)
                {
                    throw new ArgumentException("Token alınamadı");
                }

                Token = "Bearer " + JsonConvert.DeserializeObject<TokenResponseModel>(response.Content).Token;
            }

        }
        #endregion

        #region Repository
        public static void Insert(Video video)
        {
            try
            {
                var serviceProvider = new ServiceCollection()
                .AddSingleton<IRepositoryService, RepositoryService>()
                .AddDbContext<VideoDbContext>()
                .BuildServiceProvider();
                var _repositoryService = serviceProvider.GetRequiredService<IRepositoryService>();

                _repositoryService.Add<Video>(video);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
        public static void Update(Video video)
        {
            try
            {
                var serviceProvider = new ServiceCollection()
                .AddSingleton<IRepositoryService, RepositoryService>()
                .AddDbContext<VideoDbContext>()
                .BuildServiceProvider();

                var _repositoryService = serviceProvider.GetRequiredService<IRepositoryService>();

                _repositoryService.Update(video);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        #endregion

        #region Method
        public static void InsertOperation(List<Item> items)
        {
            try
            {
                var serviceProvider = new ServiceCollection()
                .AddSingleton<IRepositoryService, RepositoryService>()
                .AddDbContext<VideoDbContext>()
                .BuildServiceProvider();
                var _repositoryService = serviceProvider.GetRequiredService<IRepositoryService>();

                foreach (var item in items)
                {
                    if (item.video_paths != null && item.video_paths.Any())
                    {
                        foreach (var item1 in item.video_paths)
                        {
                            var videoDb = _repositoryService.Get<Video>(x => x.ObjectId == item._id && x.VideoPath == item1.video_path && x.Domain == CurrentDomainName);
                            if (videoDb == null)
                            {
                                try
                                {
                                    var video = new Video();
                                    video.ObjectId = item._id;
                                    video.Slug = item.slug;
                                    video.Title = item.title;
                                    video.VideoPath = item1.video_path;
                                    video.VideoType = item1.video_type;
                                    video.Label = item1.label;
                                    video.Domain = CurrentDomainName;
                                    video.SendRequest = false;
                                    video.ErstreamCheck = false;
                                    video.MediaNovaCheck = false;
                                    Insert(video);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("---Exception-----");
                                    Console.WriteLine(ex);
                                }

                            }
                            else
                            {
                                continue;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("--------");
            }

        }
        #endregion

        #region Excel
        public static void ExcelParse()
        {
            var serviceProvider = new ServiceCollection()
            .AddSingleton<IRepositoryService, RepositoryService>()
            .AddDbContext<VideoDbContext>()
            .BuildServiceProvider();
            var _repositoryService = serviceProvider.GetRequiredService<IRepositoryService>();

            //burda db'den son id'ye bakıp bu id'den büyük diyerek ilerlenebilir
            int count = _repositoryService.Count<Video>(x => x.Domain == CurrentDomainName);
            const int pageSize = 2500;
            int pageCount = count / pageSize+1;

            for (int i = 1; i <= pageCount; i++)
            {
                int skip = (i - 1) * pageSize;
                var videos = _repositoryService.GetList<Video>(x => x.Domain == CurrentDomainName, pageSize, skip);
                CreateFile(videos, $"{CurrentDomainName}-{i}.xlsx");

            }

        }
        public static void CreateFile(List<Video> videos, string fileName)
        {
            string path = $"{AppDomain.CurrentDomain.BaseDirectory}{ $"Export\\{fileName}"}";

            FileInfo file = new FileInfo(path);
            try
            {
                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(path);
                }
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    using (ExcelWorksheet worksheet = package.Workbook.Worksheets.Add($"Video Url {CurrentDomainName}"))
                    {

                        worksheet.Cells[1, 1].Value = "ObjectId";
                        worksheet.Cells[1, 2].Value = "Slug";
                        worksheet.Cells[1, 3].Value = "Title";
                        worksheet.Cells[1, 4].Value = "VideoPath";
                        worksheet.Cells[1, 5].Value = "VideoType";
                        worksheet.Cells[1, 6].Value = "Domain";

                        int i = 2;

                        Console.WriteLine("Excel liste dolduruluyor...");
                        foreach (var item in videos)
                        {
                            worksheet.Cells["A" + i].Value = item.ObjectId;
                            worksheet.Cells["B" + i].Value = item.Slug;
                            worksheet.Cells["C" + i].Value = item.Title;
                            worksheet.Cells["D" + i].Value = item.VideoPath;
                            worksheet.Cells["E" + i].Value = item.VideoType;
                            worksheet.Cells["F" + i].Value = item.Domain;
                            i++;
                        }

                        package.Save();
                    }

                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine($"ContentCount: {ex.Message}");
            }

        }
        #endregion

        ///testsorgu adlı datasource içerisine yazılan query

        // query['where'] = {
        //"status":"active",
        //"path":"/video-havuzu/"
        //}

        //query['skip'] = parameters.get("skip",0)
        //query['limit'] = parameters.get("limit",500)


        //if query["limit"] > 500:
        //  query["limit"]=500
        public static List<string> GetVs()
        {
            List<string> list = new List<string>();
            return list;
        }
    }
}
